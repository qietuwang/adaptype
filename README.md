Adaptye.js （跨屏排版）取自“Adapt”跨屏+
"Type"+排版。切图网（qietu.com）css3 media query多媒体查询快速替代方案，基于jquery的插件，压缩后仅有1k。支持最常规的分辨率下的响应，分别为电脑宽屏响应（hd），电脑普屏响应
（desktop），平板响应（flatbed），手  机响应（phone），该方案支持包括ie6~ie8在内的所有浏览器。通过加载该文件，用特定
的css技巧来书写样式即可快速实现响应，上手非常简单，  完美支持所有浏览器。
